const env = require("dotenv").config({})
const express = require("express")
const app = express()

app.use(express.json({ limit: "50mb" }))
app.get("/api", (req, res) => {
   res.send("Hello Mos")
})

app.get("/name", (req, res) => {
    res.send("")
 })

app.post("/name", (req,res) =>{
    res.send("Post name")
})

app.delete("/name", (req,res) =>{
    res.send("Delete name")
})

app.put("/name", (req,res) =>{
    res.send("Put name")
})

app.disable("x-powered-by")
app.listen(process.env.EXPRESS_PORT, async () => {
   console.log(`App listening on port ${process.env.EXPRESS_PORT}!`)
})
